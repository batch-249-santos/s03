// alert("B249!");

// In JS, classses can be created using the "class" keyword and {}.
// Naming convention for classes: Begin with Uppercase characters

/*

Syntax:
	
	class <Name> {
	
	}

*/

// Empty student class
// class Student {
// 	constructor (name, email) {
// 		// propertyname = value
// 		this.name = name;
// 		this.email = email;
// 	}
// }

// getter and setter
// Best practice dictates that we regulate acccess to such properties. We do so, via the use of "getters" (regulates retrieval) and setter (regulates manipulation)

// Method chaining

// Instantiating - process of creating objects from a class
// To create an object from a class, use the "new" keyword, when a class has a constructor,
//  we need to supply ALL the values needed by the constructor
// let studentOne = new Student ("John", "john@mail.com");
// console.log(studentOne); // Student {name: 'John', email: 'john@mail.com'}


/*

Mini - Exercise:

Create a new class called Person able to instantiate a new object with the following fields

name - 
age - shoulde be a number and must be greater than or equal to 18, otherwise set the property
to

*/

class Person {
	constructor (name, age, nationality, address) {
		this.name = name;	

		if (typeof age === "number" && age >= 18) {
			this.age = age;
		} else {
			this.age = undefined;
		}
		
		this.nationality = nationality;
		this.address = address;
	}
}

let Person1 = new Person("John", 17, "Filipino", "Manila")
let Person2 = new Person("Jane", 18, "American", "New York")

console.log(Person1)
console.log(Person2)


// Activity 1 Quiz

/*

1. class
2. Uppercase
3. new
4. Instantiating
5. constructor method

*/

// Activity 1 Function Coding

// 1. - 2.

class Student {
	constructor (name, email, grades) {
		this.name = name;
		this.email = email;
		// add this property to the constructor to store the average grade
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedwithHonors = undefined;
		if (grades.length == 4 &&
			grades.every(n => typeof n === "number") &&
			grades.every(n => n >= 0 && n <= 100)) {
			this.grades = grades;
		} else {
			this.grades = undefined;
		}
	}

	login () {
		console.log(`${this.email} has logged in.`);
		return this;
	}

	logout () {
		console.log(`${this.email} has logged out.`);
		return this;
	}

	listGrades () {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}

	computeAve () {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		// update property
		this.gradeAve = sum/4;
		// return sum/(this.grades.length)
		// return the object
		return this;
	}

	willPass () {
		this.passed = this.gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors (){
		this.passedwithHonors = this.gradeAve >= 90 ? true : false;
		return this;
	}

}


let studentOne = new Student ("John", "john@mail.com", [89, 84, 78, 88]);

// for testing 
let studentOneA = new Student ("John", "john@mail.com", [101, 84, 78, 88]);
let studentOneB = new Student ("John", "john@mail.com", [-10, 84, 78, 88]);
let studentOneC = new Student ("John", "john@mail.com", ["hello", 84, 78, 88]);
let studentOneD = new Student ("John", "john@mail.com", [84, 78, 88]);

let studentTwo = new Student ("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student ("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student ("Jessie", "jessie@mail.com", [91, 89, 92, 93]);


console.log(studentOne)

// for testing
console.log(studentOneA)
console.log(studentOneB)
console.log(studentOneC)
console.log(studentOneD)

console.log(studentTwo)
console.log(studentThree)
console.log(studentFour)



// Activity 2 Quiz

/*

1. No
2. No
3. Yes
4. Getter and Setter
5. "this" Object to be used by the next method

*/

// Activity 2 Function Coding

// 1.

// **done above**
// lines 92-94
// lines 119-137